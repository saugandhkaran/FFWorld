var app = angular.module('app',['ui.router']);

app.config([
'$stateProvider',
'$urlRouterProvider',
function($stateProvider, $urlRouterProvider) {

  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'pages/home/home.html',
      controller: 'homeCtrl'
    })
    .state('ffworld',{
      url: '/ffworld',
      templateUrl: 'pages/ffworld/ffworld.html',
      controller: 'ffworldCtrl'
    })
    .state('login',{
      url: '/login',
      templateUrl: 'pages/login/login.html',
      controller: 'loginCtrl'
    })
    .state('contactus',{
      url: '/contactus',
      templateUrl: 'pages/contactus/contactus.html',
      controller: 'contactusCtrl'
    });

  $urlRouterProvider.otherwise('/home');
}])
